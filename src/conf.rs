use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;
use std::time::Duration;

pub struct Conf {
    pub job_dir: PathBuf,
    pub output_dir: PathBuf,
    pub check_interval: Duration,
    pub driver_url: String,
}

impl Conf {
    pub fn get_default_conf() -> Conf {
        let default_driver_url = match std::env::var("HAUNTER_DRIVER_URL") {
            Err(_) => "http://localhost:4444".to_string(),
            Ok(v)  => v,
        };
        let default_job_dir = match std::env::var("HAUNTER_JOBS_DIR") {
            Err(_) => PathBuf::from("jobs.d".to_string()),
            Ok(v) => PathBuf::from(v),
        };
        let default_results_dir = match std::env::var("HAUNTER_RESULTS_DIR") {
            Err(_) => PathBuf::from("results.d".to_string()),
            Ok(v) => PathBuf::from(v),
        };
        let default_interval = match std::env::var("HAUNTER_DEFAULT_INTERVAL") {
            Err(_) => Duration::new(15*60, 0),
            Ok(v) => Duration::new(v.parse::<u64>().unwrap(), 0),
        };
        return Conf {
            job_dir: default_job_dir,
            output_dir: default_results_dir,
            check_interval: default_interval,
            driver_url: default_driver_url,
        };
    }

    pub fn update_from_file(&mut self, path: &Path) {
        let items = match read_conf_file(path) {
            Err(_) => return,
            Ok(items) => items,
        };
        for item in items.iter() {
            let key = item.0.as_str();
            let value = item.1.as_str();
            match key {
                "job_dir" => {
                    println!("{} changed from '{}' to '{}' by line in '{}'",
                             key, self.job_dir.display(), value, path.display());
                    self.job_dir = PathBuf::from_str(value).unwrap();
                },
                "output_dir" => {
                    println!("{} changed from '{}' to '{}' by line in '{}'",
                             key, self.output_dir.display(), value, path.display());
                    self.output_dir = PathBuf::from_str(value).unwrap();
                },
                "check_interval" => {
                    let converted_value = match value.parse::<u64>() {
                        Err(why) => { println!("Failed to convert '{}' to u64: {}", value, why); continue;},
                        Ok(v) => v,
                    };
                    println!("{} changed from '{}' to '{}' by line in '{}'", key, self.check_interval.as_secs(), value, path.display());
                    self.check_interval = Duration::new(converted_value, 0);
                },
                "driver_url" => {
                    println!("{} changed from '{}' to '{}' by line in '{}'", key, self.driver_url, value, path.display());
                    self.driver_url = String::from_str(value).unwrap();
                }
                _ => {
                    println!("Unknown key '{}' in file '{}'", key, path.display());
                }
            }
        }
    }
}

pub fn read_conf_file(path: &Path) -> Result<std::vec::Vec<(String, String)>, &str> {
    let mut file = match std::fs::File::open(path) {
        Err(why) => {
            println!("Could not open file '{}': {}", path.display(), why);
            return Err("Could not open file");
        },
        Ok(file) => file,
    };
    let mut content = String::new();
    match file.read_to_string(&mut content) {
        Err(why) => {
            println!("Could not read from file '{}': {}", path.display(), why);
            return Err("Could not read file");
        },
        Ok(_) => (),
    }

    let mut results = std::vec::Vec::<(String, String)>::new();
    let lines = content.lines();
    for line in lines {
        if line.starts_with('#') {
            continue;
        }
        let result = line.split_once('=');
        if result.is_none() {
            println!("Skipping configuration line '{}', no key-value delimiter (=) found", line);
            continue;
        }

        let key = result.unwrap().0.trim();
        let value = result.unwrap().1.trim();
        if key.eq("") || value.eq("") {
            println!("Skipping configuration line '{}', no key or value side is empty", line);
            continue;
        }
        results.push((key.to_string(), value.to_string()));
    }
    return Ok(results);
}

#[cfg(test)]
mod tests {

    use std::io::Write;
    use std::str::FromStr;
    use tempfile::NamedTempFile;

    use super::*;

    fn write_conf_to_temp_file(content: &str) -> NamedTempFile {
        let mut f = NamedTempFile::new().unwrap();
        f.write_all(content.as_bytes()).expect("Failed to write configuration to file");
        return f;
    }

    #[test]
    fn test_not_existing() {
        let mut conf = Conf::get_default_conf();
        conf.update_from_file(Path::new("/not/a/real/file"));
    }

    #[test]
    fn test_set_all_values() {
        let mut conf = Conf::get_default_conf();
        let tf = write_conf_to_temp_file(r#"
job_dir = /val=ue
# this line is ignored, and the white space on the next line is intentional
output_dir=/var/lib/output  
check_interval=3600
driver_url = https://example.test:6666
        "#);
        conf.update_from_file(tf.path());
        assert!(Path::new("/val=ue").eq(&conf.job_dir));
        assert!(Path::new("/var/lib/output").eq(&conf.output_dir));
        assert!(String::from_str("https://example.test:6666").unwrap().eq(&conf.driver_url));
        assert_eq!(3600, conf.check_interval.as_secs());
    }

    #[test]
    fn test_non_numeric_check_interval_fails() {
        let mut conf = Conf::get_default_conf();
        let tf = write_conf_to_temp_file(r#"
check_interval=d3600
        "#);
        conf.update_from_file(tf.path());
        assert_eq!(15*60, conf.check_interval.as_secs());

    }
}
