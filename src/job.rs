use std::path::{Path,PathBuf};
use std::str::FromStr;
use std::time::Duration;
use std::time::Instant;

use ansi_to_html;
use chrono;
use rss;
use scraper;

use crate::conf::Conf;

pub struct Job {
    pub url: String,
    pub selector: String,
    pub every: Duration,
    pub last_run: Option<Instant>,
    pub output_file: Option<PathBuf>,
    pub channel: Option<rss::Channel>,
    pub source_file: Option<PathBuf>,
}

impl Job {

    fn default(conf: &Conf) -> Job {
        return Job {
            url: "".to_string(),
            selector: "".to_string(),
            every: conf.check_interval,
            last_run: None,
            output_file: None,
            channel: None,
            source_file: None,
        };
    }

    fn set_default_output_file(&mut self, conf: &Conf) {
        let mut output_file = conf.output_dir.clone();
        let mut file_name = self.url.clone().replace("/", "-");
        file_name.push_str(".rss");
        output_file = output_file.join(Path::new(&file_name));
        self.output_file = Some(output_file);
    }

    fn set_default_channel(&mut self) {
        match std::fs::File::open(self.output_file.as_ref().unwrap()) {
            Err(why) => {
                println!("Failed to open '{}': {}", self.output_file.as_ref().unwrap().display(), why);
                println!("Creating empty RSS channel for job '{}'", self.url);
                self.channel = Some(
                    rss::ChannelBuilder::default()
                        .title(self.url.as_str())
                        .link(self.url.as_str())
                        .description("haunting")
                        .build()
                );
                self.channel.as_mut().unwrap().set_generator("Haunter".to_string());
            },
            Ok(file) => {
                println!("Reading channel from '{}'", self.output_file.as_ref().unwrap().display());
                self.channel = Some(
                    rss::Channel::read_from(std::io::BufReader::new(file)).unwrap()
                );
            },
        };
    }

    pub fn has_job_file_extension(path: &Path) -> bool {
        match path.extension() {
            Some(x) => {
                if ! "job".eq(x) {
                    return false;
                }
            },
            None => {
                return false;
            }
        };
        return true;
    }

    pub fn new(url: &str, selector: &str, conf: &Conf) -> Job {
        let mut job = Job::default(conf);
        job.url = url.to_string();
        job.set_default_output_file(conf);
        job.selector = selector.to_string();
        job.set_default_channel();
        return job;
    }

    pub fn from_file<'a>(path: &'a Path, conf: &'a Conf) -> Result<Job, &'a str> {
        let mut job = Job::default(conf);
        let items = match crate::conf::read_conf_file(path) {
            Err(_) => return Err("Failed to read from configuration file"),
            Ok(items) => items,
        };
        for item in items.iter() {
            let key = item.0.as_str();
            match key {
                "url" => {
                    job.url = item.1.clone();
                },
                "selector" => {
                    job.selector = item.1.clone();
                },
                "every" => {
                    let converted_value = match item.1.parse::<u64>() {
                        Err(why) => {
                            println!("Failed to convert '{}' to u64: {}", item.1, why);
                            return Err("Failed to parse value of 'every'");
                        },
                        Ok(v) => v,
                    };
                    job.every = Duration::new(converted_value, 0);
                },
                "output_file" => {
                    if item.1.starts_with("/") || item.1.starts_with("./") {
                        job.output_file = Some(
                            PathBuf::from_str(item.1.as_str()).unwrap()
                        );
                    }
                    else {
                        job.output_file = Some(
                            conf.output_dir.join(PathBuf::from_str(item.1.as_str()).unwrap())
                        );
                    }
                }
                _ => {
                    println!("Unknown key '{}' in job file '{}'", key, path.display());
                    return Err("Unknown key");
                }
            }
        }
        if job.output_file.is_none() {
            job.set_default_output_file(conf);
        }
        job.set_default_channel();
        job.source_file = Some(PathBuf::from(path));
        return Ok(job);
    }

    pub fn last_value(&self) -> Option<String> {
        if self.channel.is_none() {
            return None;
        }
        let channel = self.channel.as_ref().unwrap();
        if channel.items.len() == 0 {
            return None;
        }
        let last = &channel.items[channel.items.len()-1];
        if last.content.is_none() {
            return None;
        }
        let fragment = scraper::Html::parse_fragment(&last.content.as_ref().unwrap().as_str());
        let selector = scraper::Selector::parse("pre.new-value").unwrap();
        let value = match fragment.select(&selector).next() {
            Some(value) => value,
            _ => {
                return None;
            },
        };
        return Some(value.inner_html().trim().to_string());
    }

    pub fn update(&mut self, value: &str, diff: &str) {
        if self.channel.is_none() {
            println!("Skipping update of channel: no channel set");
            return;
        }
        let channel = self.channel.as_mut().unwrap();
        let update_time = chrono::Utc::now();
        let mut guid = rss::Guid::default();
        guid.set_permalink(false);
        guid.set_value(update_time.timestamp().to_string().as_str());
        let item = rss::ItemBuilder::default()
            .title(format!("Update to '{}'", self.url))
            .link(self.url.clone())
            .pub_date(update_time.to_rfc2822())
            .guid(Some(guid))
            .content(format!(r#"
New content at {}: <br>
<pre class="new-value">{}</pre>
<br><br>
Diff: <br>
<pre class="diff-value">{}</pre>
"#,
                             update_time.format("%d/%m/%Y %H:%M"),
                             value,
                             ansi_to_html::convert_escaped(diff).unwrap().as_str()
                )
            )
            .build();

        channel.items.push(item);

        if self.output_file.is_some() {
            match std::fs::File::create(self.output_file.as_ref().unwrap()) {
                Err(why) => {
                    println!("Failed to open '{}' for writing: {}", self.output_file.as_ref().unwrap().display(), why);
                },
                Ok(file) => {
                    channel.write_to(
                        std::io::BufWriter::new(file)
                    ).expect("Failed to write updated channel");
                },
            };
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn create() {
        let conf = Conf::get_default_conf();
        let job = Job::new(&"my/url", &"myselector", &conf);
        assert_eq!(job.url, "my/url");
        assert_eq!(job.output_file.unwrap().to_str().unwrap(), "results.d/my-url.rss");
        assert_eq!(job.selector, "myselector");
        assert!(job.source_file.is_none());
        assert!(job.channel.is_some());
    }

    #[test]
    fn create_from_file() {
        let conf = Conf::get_default_conf();
        let mut tf = NamedTempFile::new().unwrap();
        let job_conf = r#"
url = http://example.com/test
output_file = example_output.atom
every=7200

selector = section.listing:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > header:nth-child(3) > h2:nth-child(1) > a:nth-child(1)
"#;
        tf.write_all(job_conf.as_bytes()).expect("Failed to write configuration to file");

        let job = Job::from_file(tf.path(), &conf).expect("Failed to read configuration file");
        assert_eq!(job.url, "http://example.com/test");
        assert_eq!(job.output_file.unwrap().to_str().unwrap(), "results.d/example_output.atom");
        assert_eq!(job.every.as_secs(), 7200);
        assert_eq!(job.selector, "section.listing:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > header:nth-child(3) > h2:nth-child(1) > a:nth-child(1)");
        assert_eq!(job.source_file.unwrap().to_str(), tf.path().to_str());
        assert!(job.channel.is_some());
    }

    #[test]
    fn create_from_file_default_output_file() {
        let conf = Conf::get_default_conf();
        let mut tf = NamedTempFile::new().unwrap();
        let job_conf = r#"
url = http://example.com/test
every=7200

selector = section.listing:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > header:nth-child(3) > h2:nth-child(1) > a:nth-child(1)
"#;
        tf.write_all(job_conf.as_bytes()).expect("Failed to write configuration to file");

        let job = Job::from_file(tf.path(), &conf).expect("Failed to read configuration file");
        assert_eq!(job.url, "http://example.com/test");
        assert_eq!(job.output_file.unwrap().to_str().unwrap(), "results.d/http:--example.com-test.rss");
        assert_eq!(job.every.as_secs(), 7200);
        assert_eq!(job.selector, "section.listing:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > header:nth-child(3) > h2:nth-child(1) > a:nth-child(1)");
        assert_eq!(job.source_file.unwrap().to_str(), tf.path().to_str());
        assert!(job.channel.is_some());
    }

    #[test]
    fn recover_value_from_channel() {
        let conf = Conf::get_default_conf();
        let mut job_file = NamedTempFile::new().unwrap();
        let job_conf = format!(r#"
url = http://example.com/test
output_file = ./src/job_example.rss
"#);
        job_file.write_all(job_conf.as_bytes()).expect("Failed to write job test content");
        let job = Job::from_file(job_file.path(), &conf).expect("Failed to read configuration file");
        assert_eq!(job.output_file.as_ref().unwrap().to_str().unwrap(), "./src/job_example.rss");
        assert_eq!(job.last_value().unwrap(), "Version 1.64.0");
    }

    #[test]
    fn recover_value_from_channel_html_escapes() {
        let conf = Conf::get_default_conf();
        let output_file = NamedTempFile::new().unwrap();
        let mut job_file = NamedTempFile::new().unwrap();
        let mut job = Job::new("example", "selector", &conf);
        job.output_file = Some(output_file.path().to_path_buf());
        writeln!(job_file, "url = example").expect("write failed");
        writeln!(job_file, "output_file = {}", output_file.path().display()).expect("write failed");
        let value = "Update that contains <a href=\"#\">html</a><br>";
        job.update(value, "diff");
        let job2 = Job::from_file(job_file.path(), &conf).expect("Failed to read conf file");
        assert_eq!(job2.last_value().unwrap(), value);
    }

    #[test]
    fn new_updates_have_different_guids() {
        let conf = Conf::get_default_conf();
        let mut job = Job::new("example", "selector", &conf);
        job.output_file = None;
        job.update("update 1", "diff");
        std::thread::sleep(Duration::new(1, 0));
        job.update("update 2", "diff");
        assert!(job.channel.as_ref().unwrap().items[0].guid.is_some());
        assert!(job.channel.as_ref().unwrap().items[1].guid.is_some());
        assert_ne!(job.channel.as_ref().unwrap().items[0].guid.as_ref().unwrap(),
                   job.channel.as_ref().unwrap().items[1].guid.as_ref().unwrap());
    }
}
